Windows:
* Save selected bluetooth device
* Make bluetooth devices combo box respect theme
* Detect screen saver / session lock to consider PC idle
* Resolve interference idle detection / sleep prevention
* Remove 32feet.NET header from debug output
* If message failed to send retry a bit later

Android:
* Tap notification highlights message
* Investigate startup error "BluetoothAdapter: getBluetoothService() called with no BluetoothManagerCallback"
* Service must report to activity that bluetooth isn't available

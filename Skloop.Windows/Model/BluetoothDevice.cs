﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using InTheHand.Net.Sockets;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;
using System.Globalization;
using InTheHand.Net;

namespace Skloop.Model
{
    public class BluetoothAddressToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is BluetoothAddress)
            {
                return string.Format("{0:C}", (BluetoothAddress)value);
            }
            else return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class BluetoothDevices : ObservableCollection<BluetoothDeviceInfo>, INotifyPropertyChanged 
    {
        private BluetoothDeviceInfo selectedDevice;

        public BluetoothDeviceInfo SelectedDevice
        {
            get { return selectedDevice; }
            set
            {
                selectedDevice = value;
                NotifyPropertyChanged("SelectedDevice");
                NotifyPropertyChanged("SelectedDeviceIndex");
            }
        }

        public int SelectedDeviceIndex
        {
            get
            {
                for (int i = 0; i < Count; i++)
                {
                    BluetoothDeviceInfo bdi = this[i];
                    if (bdi.DeviceAddress == SelectedDevice.DeviceAddress)
                        return i;
                }
                return -1;
            }
            set
            {
                int i = value;
                if (i >= 0) SelectedDevice = this[i];
            }
        }

        public BluetoothDevices()
        {
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                BluetoothDeviceInfo bti = new BluetoothDeviceInfo(new InTheHand.Net.BluetoothAddress(new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06 }));
                bti.DeviceName = "Device 1";
                Add(bti);
                bti = new BluetoothDeviceInfo(new InTheHand.Net.BluetoothAddress(new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06 }));
                bti.DeviceName = "Device 2";
                Add(bti);
                bti = new BluetoothDeviceInfo(new InTheHand.Net.BluetoothAddress(new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06 }));
                bti.DeviceName = "Device 3";
                Add(bti);
                SelectedDevice = bti;
            }
        }

        protected override event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class GotListOfPairedDevicesEventArgs : EventArgs
    {
        public BluetoothDevices Devices { get; internal set; }
        public GotListOfPairedDevicesEventArgs(BluetoothDevices devices)
        {
            Devices = devices;
        }
    }
}

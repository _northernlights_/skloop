﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using Hardcodet.Wpf.TaskbarNotification;
using InTheHand.Net.Sockets;
using Skloop.Logic;
using Skloop.Model;
using Skloop.Shared.Model;
using Skloop.UI;
using System.Windows;

namespace Skloop
{

    public partial class App : Application
    {
        public static App Instance { get; internal set; }

        private OutlookMonitor toutlookMonitor;
        private LyncMonitor lyncMonitor;
        private BluetoothScanner btScanner;
        private BluetoothDevices btDevices;
        private MainWindow mainWindow;
        private Insomnia insomnia;
        private IdleDetection idleDetection;
        private HeartBeatSender heartBeatSender;
        private Properties.Settings settings;

        public App()
        {
            Instance = this;

            settings = Skloop.Properties.Settings.Default;
            settings.PropertyChanged += Settings_PropertyChanged;
            btDevices = new BluetoothDevices();
            mainWindow = new UI.MainWindow();
            mainWindow.Closing += MainWindow_Closing;
            mainWindow.Closed += MainWindow_Closed;
            mainWindow.BluetoothDevicesListContext = btDevices;
        }

        private void MainWindow_Closed(object sender, System.EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (settings.CloseToTray)
            {
                MainWindow.Hide();
                e.Cancel = true;
            }
            else
            {
                mainWindow.SystrayIcon.Visibility = Visibility.Collapsed;
            }
        }

        private void Settings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            settings.Save();

            if (e.PropertyName == "InsomniaOn")
            {
                if (settings.InsomniaOn)
                {
                    insomnia = new Insomnia();
                    insomnia.Start();
                }
                else
                {
                    insomnia.Stop();
                    insomnia = null;
                }
            }
        }

        public void RefreshBluetoothDevices()
        {
            mainWindow.BluetoothRefresh.IsEnabled = false;
            btScanner.StartScanningPairedDevices();
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            idleDetection = new IdleDetection(settings.IdleDelay);
            toutlookMonitor = new OutlookMonitor();
            toutlookMonitor.GotNewMessage += GotNewMessage;
            lyncMonitor = new LyncMonitor();
            lyncMonitor.GotNewMessage += GotNewMessage;
            heartBeatSender = new HeartBeatSender(Constants.HEARTBEATDELAY);
            heartBeatSender.GotNewMessage += GotNewMessage;

            btScanner = new BluetoothScanner();
            btScanner.GotListOfPairedDevices += BTScanner_GotListOfPairedDevices;
            RefreshBluetoothDevices();

            if (!settings.StartMinimized) mainWindow.Show();
            else mainWindow.SystrayIcon.ShowBalloonTip("Skloop", "Monitoring Lync/Skype for Business and Outlook", BalloonIcon.Info);

            mainWindow.MinHeight = mainWindow.ActualHeight;
            mainWindow.InsomniaCheckbox.IsChecked = settings.InsomniaOn;
        }

        private void BTScanner_GotListOfPairedDevices(object sender, GotListOfPairedDevicesEventArgs e)
        {
            //remove previous bluetooth devices if any then add the new ones
            if (btDevices.Count > 0)
            {
                for (int i = btDevices.Count; i > 0; i--)
                {
                    btDevices.RemoveAt(i - 1);
                }
            }
            foreach (BluetoothDeviceInfo device in e.Devices)
            {
                btDevices.Add(device);
            }
            //select first one automatically
            if (btDevices.Count > 0) btDevices.SelectedDeviceIndex = 0;

            mainWindow.BluetoothRefresh.IsEnabled = true;
        }

        private void GotNewMessage(object sender, Monitor.GotNewMessageEventArgs e)
        {
            if ((settings.OnlyWhenIdle && idleDetection.IsIdle) || !settings.OnlyWhenIdle)
            {
                if (btDevices.SelectedDevice != null)
                {
                    new BluetoothNotifier().Notify(btScanner.LocalBTAddress, btDevices.SelectedDevice.DeviceAddress, e.Message);
                }
            }
        }

    }
}

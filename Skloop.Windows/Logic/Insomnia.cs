﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

// Everything's a copy of a copy of a copy.

namespace Skloop.Logic
{
    public class Insomnia : IDisposable
    {
        private static int delay = 59; //key event every 59 s.
        private bool stop = false;

        public void Start()
        {
            Debug.WriteLine("Sleep prevention started");
            stop = false;
            new Thread(delegate ()
            {
                while (!stop)
                {
                    Thread.Sleep(delay * 1000);
                    GenerateKeyEvent();
                }
            }).Start();
        }

        public void Stop()
        {
            Debug.WriteLine("Sleep prevention stopped");
            stop = true;
        }

        public void Dispose()
        {
            Stop();
        }

        private void GenerateKeyEvent()
        {
            keybd_event(VK_F15, 0, KEYEVENTF_KEYUP, 0);
        }

        private const int VK_F15 = 0x7E; //F15 key
        private const uint KEYEVENTF_KEYUP = 0x0002; //key released

        [DllImport("user32.dll")]
        public static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, uint dwExtraInfo);
    }
}

﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;
using Skloop.Model;
using Skloop.Shared.Model;
using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;

namespace Skloop.Logic
{
    public class BluetoothNotifier
    {
        public void Notify(BluetoothAddress localAddress, BluetoothAddress address, Message message)
        {
            string serializedMessage = ObjectSerializer<Message>.ToXml(message);
            if (message.Source != SourceApp.Heartbeat)
                Debug.WriteLine("Serialized message: " + Environment.NewLine + serializedMessage);

            new Thread(delegate ()
            {
                using (BluetoothClient btClient = new BluetoothClient(new BluetoothEndPoint(localAddress, BluetoothService.SerialPort)))
                {
                    try
                    {
                        btClient.Connect(address, new Guid(Constants.BLUETOOTH_SERVER_GUID));
                    }
                    catch (Exception oops)
                    {
                        Debug.WriteLine("Failed to connect bluetooth socket: " + oops.Message);
                    }
                    if (btClient.Connected)
                    {
                        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(serializedMessage);
                        NetworkStream s = btClient.GetStream();
                        s.Write(bytes, 0, bytes.Length);
                        s.Flush();
                    }
                }
            }).Start();
        }
    }
}

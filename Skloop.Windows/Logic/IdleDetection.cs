﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

namespace Skloop.Logic
{
    public class IdleDetection : IDisposable
    {
        public int Delay { get; internal set; } //in minutes
        public bool IsIdle { get; internal set; }
        private bool stop;

        public IdleDetection(int delay)
        {
            Delay = delay;
            stop = false;
            IsIdle = false;

            new Thread(delegate ()
            {
                while (!stop)
                {
                    tagLASTINPUTINFO lastInput = new tagLASTINPUTINFO();
                    int idleTime;
                    lastInput.cbSize = (uint)Marshal.SizeOf(lastInput);
                    lastInput.dwTime = 0;

                    if (GetLastInputInfo(ref lastInput))
                    {
                        bool lastValue = IsIdle;
                        idleTime = Environment.TickCount - lastInput.dwTime; //in ms
                        if (idleTime >= Delay * 1000 * 60) IsIdle = true;
                        else IsIdle = false;

                        if (IsIdle != lastValue)
                        {
                            if (IsIdle) Debug.WriteLine("PC is idle");
                            else Debug.WriteLine("PC is active");
                        }
                    }

                    Thread.Sleep(10 * 1000); //check again in 10s
                }
            }).Start();
        }

        public void Dispose()
        {
            stop = true;
        }

        //http://forum.codecall.net/topic/69801-user-idle-detection/
        [DllImport("user32.dll")]
        public static extern bool GetLastInputInfo(ref tagLASTINPUTINFO plii);

        public struct tagLASTINPUTINFO
        {
            public uint cbSize;
            public int dwTime;
        }
    }
}

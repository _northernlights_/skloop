﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using Skloop.Model;
using System;

namespace Skloop.Logic
{
    public class Monitor
    {
        public event EventHandler<GotNewMessageEventArgs> GotNewMessage;
        public event EventHandler<GotErrorEventArgs> GotError;

        protected void RaiseGotNewMessage(Message m)
        {
            GotNewMessage?.Invoke(this, new GotNewMessageEventArgs(m));
        }

        protected void RaiseGotError(Error e)
        {
            System.Diagnostics.Debug.WriteLine("Error: " + e.Message);
            GotError?.Invoke(this, new GotErrorEventArgs(e));
        }

        public class GotNewMessageEventArgs : EventArgs
        {
            public Message Message { get; internal set; }
            public GotNewMessageEventArgs(Message m)
            {
                Message = m;
            }
        }

        public class GotErrorEventArgs : EventArgs
        {
            public Error Error { get; internal set; }
            public GotErrorEventArgs(Error error)
            {
                Error = error;
            }
        }

        public class Error : Exception
        {
            public SourceApp SourceApp { get; internal set; }

            public Error(string message, SourceApp source) : base(message)
            {
                SourceApp = source;
            }
        }
    }
}

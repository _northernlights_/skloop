﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using Skloop.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Skloop.Logic
{
    public class OutlookMonitor : Monitor
    {
        private Outlook.Application app = null;
        private Outlook._NameSpace ns = null;
        private Outlook.MAPIFolder inboxFolder = null;
        private Outlook.Folder subFolder = null;

        //http://stackoverflow.com/a/12303175
        private List<Outlook.Items> monitoredFolderItems = new List<Outlook.Items>();
        private Outlook.Reminders reminders;

        public OutlookMonitor()
        {
            try
            {
                app = new Outlook.Application();
                ns = app.GetNamespace("MAPI");
                ns.Logon(null, null, false, false);

                inboxFolder = ns.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox);
                subFolder = (Outlook.Folder)inboxFolder;
                Debug.WriteLine(string.Format("Outlook Folder Name: {0}, EntryId: {1}", subFolder.Name, subFolder.EntryID));
                Debug.WriteLine(string.Format("Num Items: {0}", subFolder.Items.Count.ToString()));

                monitoredFolderItems.Add(subFolder.Items);
                monitoredFolderItems[0].ItemAdd += ItemAdded;
                for (int i = 1; i < subFolder.Folders.Count + 1; i++)
                {
                    monitoredFolderItems.Add((subFolder.Folders[i] as Outlook.Folder).Items);
                    monitoredFolderItems[i - 1].ItemAdd += ItemAdded;
                }

                reminders = app.Reminders;
                reminders.ReminderFire += Reminders_ReminderFire;
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                ns = null;
                app = null;
                inboxFolder = null;
                subFolder = null;

                RaiseGotError(new Error("Could not connect to Outlook: " + ex.Message, SourceApp.Outlook));
            }
        }

        private void Reminders_ReminderFire(Outlook.Reminder ReminderObject)
        {
            Message mess = new Message(
                SourceApp.Reminder,
                string.Empty,
                ReminderObject.Caption,
                string.Format("{0} {1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString()));
            Debug.WriteLine("!!! Reminder !!!" + Environment.NewLine + mess.ToString());
            RaiseGotNewMessage(mess);
        }

        private void ItemAdded(object item)
        {
            try
            {
                Outlook.MailItem msg = item as Outlook.MailItem;
                Message mess = new Message(
                    SourceApp.Outlook,
                    string.Format("{0} <{1}>", msg.SenderName, msg.SenderEmailAddress),
                    msg.Subject,
                    string.Format("{0} {1}", msg.SentOn.ToLongDateString(), msg.SentOn.ToLongTimeString()));
                Debug.WriteLine("!!! Received new email !!!" + Environment.NewLine + mess.ToString());
                RaiseGotNewMessage(mess);
            }
            catch (Exception oops)
            {
                Debug.WriteLine("Could not parse email: " + oops.ToString());
            }
        }        

    }

}

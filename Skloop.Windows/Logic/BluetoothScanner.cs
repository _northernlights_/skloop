﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;
using Skloop.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Skloop.Logic
{
    public class BluetoothScanner
    {
        private BluetoothComponent localComponent;
        private List<BluetoothDeviceInfo> pairedDevices;

        public  BluetoothAddress LocalBTAddress { get; internal set; }
        public event EventHandler<GotListOfPairedDevicesEventArgs> GotListOfPairedDevices;

        public BluetoothScanner()
        {
            BluetoothRadio myRadio = BluetoothRadio.PrimaryRadio;
            if (myRadio == null)
            {
                Debug.WriteLine("No radio hardware or unsupported software stack");
                throw new NoBluetoothHardwareDetectedException();
            }
            RadioMode mode = myRadio.Mode;
            if (myRadio.LocalAddress == null) throw new BluetoothIsOffException();
            Debug.WriteLine("Found bluetooth adapter with address: {0:C}", myRadio.LocalAddress);
           LocalBTAddress = myRadio.LocalAddress;


            localComponent = new BluetoothComponent(new BluetoothClient(new BluetoothEndPoint(LocalBTAddress, BluetoothService.SerialPort)));
            localComponent.DiscoverDevicesProgress += new EventHandler<DiscoverDevicesEventArgs>(component_DiscoverDevicesProgress);
            localComponent.DiscoverDevicesComplete += new EventHandler<DiscoverDevicesEventArgs>(component_DiscoverDevicesComplete);
        }

        public void StartScanningPairedDevices()
        {
            pairedDevices = new List<BluetoothDeviceInfo>();
            localComponent.DiscoverDevicesAsync(255, true, true, true, false, null);
        }

        private void component_DiscoverDevicesProgress(object sender, DiscoverDevicesEventArgs e)
        {
            for (int i = 0; i < e.Devices.Length; i++)
            {
                if (e.Devices[i].Authenticated)
                {
                    Debug.WriteLine("Found paired bluetooth device with name " + e.Devices[i].DeviceName + " and address {0:C}", e.Devices[i].DeviceAddress);
                    pairedDevices.Add(e.Devices[i]);
                }
            }
        }

        private void component_DiscoverDevicesComplete(object sender, DiscoverDevicesEventArgs e)
        {
            BluetoothDevices devices = new BluetoothDevices();
            foreach (BluetoothDeviceInfo bdi in pairedDevices)
            {
                devices.Add(bdi);
            }
            RaiseGotListOfPairedDevices(devices);
        }

        private void RaiseGotListOfPairedDevices(BluetoothDevices devices)
        {
            GotListOfPairedDevices?.Invoke(this, new GotListOfPairedDevicesEventArgs(devices));
        }
    }

    public class NoBluetoothHardwareDetectedException : Exception { }
    public class BluetoothIsOffException : Exception { }
}

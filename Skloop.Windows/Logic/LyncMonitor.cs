﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using Microsoft.Lync.Model;
using Microsoft.Lync.Model.Conversation;
using System;
using System.Diagnostics;

namespace Skloop.Logic
{
    public class LyncMonitor : Monitor
    {
        private LyncClient lyncClient;

        //https://rcosic.wordpress.com/2011/10/27/sign-in-and-out-in-microsoft-lync-client/
        public LyncMonitor()
        {
            try
            {
                lyncClient = LyncClient.GetClient();
            }
            catch (ClientNotFoundException)
            {
                RaiseGotError(new Error("Lync is not installed or not running", Model.SourceApp.Lync));
            }

            if (lyncClient != null)
            {
                if (lyncClient.InSuppressedMode)
                {
                    if (lyncClient.State == ClientState.Uninitialized)
                    {
                        Debug.WriteLine("Lync is not initialized; initializing");
                        object[] asyncState = { lyncClient };
                        IAsyncResult asyncResult = lyncClient.BeginInitialize(InitializeCallback, asyncState);
                        asyncResult.AsyncWaitHandle.WaitOne();
                    }
                }

                if (lyncClient.State == ClientState.SignedIn)
                {
                    Debug.WriteLine("Lync is signed in");
                    //ready to subscribe to conversation events
                    //InstantMessageReceived is contained within modalities, which are contained within participants,
                    //which are contained within conversations; that means maintaining a complicated nested list of events
                    //so for now i'm just monitoriong new conversations
                    lyncClient.ConversationManager.ConversationAdded += ((object sender, ConversationManagerEventArgs e)
                        =>
                    {
                        //if there's only one participant it means the user just started a new conversation themself
                        if (e.Conversation.Participants.Count > 1)
                        {
                            Model.Message m = new Model.Message();
                            m.Source = Model.SourceApp.Lync;
                            m.Sent = DateTimeOffset.Now.ToString();
                            m.Subject = "New conversation";

                            //the first participant is the user; report the 2nd one as the initiator
                            m.From = e.Conversation.Participants[1].Contact.Uri.ToString();

                            Debug.WriteLine("!!! New Lync conversation !!!" + Environment.NewLine + m.ToString());
                            RaiseGotNewMessage(m);
                        }
                    });
                }
                else
                {
                    RaiseGotError(new Error("Lync is not signed in", Model.SourceApp.Lync));
                }
            }
        }

        private void InitializeCallback(IAsyncResult result)
        {
            if (result.IsCompleted)
            {
                object[] asyncState = (object[])result.AsyncState;
                ((LyncClient)asyncState[0]).EndInitialize(result);
            }
        }
    }
}

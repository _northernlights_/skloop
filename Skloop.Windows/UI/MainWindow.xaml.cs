﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using System;
using System.Diagnostics;
using System.Windows;

namespace Skloop.UI
{
    public partial class MainWindow : Window
    {
        public DebugWindow DebugWindow { get; internal set; }

        public object BluetoothDevicesListContext
        {
            set
            {
                BluetoothList.DataContext = value;
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            
            DebugWindow = new DebugWindow();
            TextBoxStreamWriter textBoxStreamWriter = new TextBoxStreamWriter(DebugWindow.OutputBox);
            Console.SetOut(textBoxStreamWriter);
            Console.SetError(textBoxStreamWriter);
            Debug.Listeners.Add(new DebugTraceListener(textBoxStreamWriter));
        }

        private void DebugButton_Click(object sender, RoutedEventArgs e)
        {
            DebugWindow.Show();
        }

        private class DebugTraceListener : TraceListener
        {
            TextBoxStreamWriter tbStreamWriter;

            public DebugTraceListener(TextBoxStreamWriter textBoxStreamWriter)
            {
                tbStreamWriter = textBoxStreamWriter;
            }

            public override void Write(string message)
            {
                tbStreamWriter.Write(message);
            }

            public override void WriteLine(string message)
            {
                tbStreamWriter.WriteLine(message);
            }
        }

        private void BluetoothRefresh_Click(object sender, RoutedEventArgs e)
        {
            App.Instance.RefreshBluetoothDevices();
        }

        private void RestoreSystrayItem_Click(object sender, RoutedEventArgs e)
        {
            Show();
        }

        private void QuitSystrayItem_Click(object sender, RoutedEventArgs e)
        {
            App.Instance.Shutdown();
        }

        private void BluetoothList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //TODO
        }

        private void SystrayIcon_TrayMouseDoubleClick(object sender, RoutedEventArgs e)
        {
            Show();
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            new AboutDialog().ShowDialog();
        }
    }
}

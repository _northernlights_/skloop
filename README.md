# Skloop #
*Skype for Business / Lync and Outlook monitoring via bluetooth*

### What is this? ###

This program forwards Outlook and Skype for Business / Lync messages from a PC to an Android device via Bluetooth. The typical use case is receiving your messages on your mobile device while in the office in a work place that doesn't allow mobile devices to sync over Internet but allows Bluetooth.

It consists of 2 parts:

* The Windows program that monitors Skype for Business / Lync and Outlook and sends to the Android device

* The Android program acting as a Bluetooth server in a service showing you notifications when messages are received

### Screenshots ###

Windows piece:

![screenshot00060.png](https://bitbucket.org/repo/MB9qoM/images/3976522068-screenshot00060.png)

Android piece:

![android2_resize.png](https://bitbucket.org/repo/MB9qoM/images/3812922818-android2_resize.png)

### License ###

Skloop is Free:

* doesn't cost money

* has no ads

* source code is available and can be re-used following the terms of the very permissive [MIT license](https://en.wikipedia.org/wiki/MIT_License)

### Download ###

Both the Windows and Android parts can be downloaded [here](https://bitbucket.org/_northernlights_/skloop/downloads). The Android part can also be [downloaded from the Google Play store](https://play.google.com/store/apps/details?id=fr.lorteau.skloop).

### Installation ###

The Windows part doesn't need installation and has no dependencies; it's one portable .exe file. Just run it. The Android part is installed like any other Android program: open the .apk on your device or use the [Google Play store](https://play.google.com/store/apps/details?id=fr.lorteau.skloop).

### Privacy Policy ###

Skloop doesn't collect any of your credentials. Outlook and Skype / Lync need to be running for the Skloop PC component to be able to hook to their events and forwards their messages. The Android application only stores messages locally and is never sent any credentials. The messages are also never sent over Internet; only via Bluetooth to a pre-paired and selected bluetooth device.
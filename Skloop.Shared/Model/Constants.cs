﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//


namespace Skloop.Shared.Model
{
    public static class Constants
    {
        public static int HEARTBEATDELAY = 5; //seconds
        public static string SOURCECODE_LINK = "https://bitbucket.org/_northernlights_/skloop/src";
        public static string LICENSE = @"
Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/ or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.";
        public static string BLUETOOTH_SERVER_GUID = "f96a19a1-93f8-47e8-9d48-f45a40ee6474";
        public static string STARTFOREGROUND_ACTION = "fr.lorteau.skloop.foregroundservice.action.start";
        public static string STOPFOREGROUND_ACTION = "fr.lorteau.skloop.foregroundservice.action.stop";
        public static string DISMISS_ACTION = "fr.lorteau.skloop.foregroundservice.action.dismiss";
        public static string MAIN_ACTION = "fr.lorteau.skloop.action.main";
        public static string LOST_CONTACT_ACTION = "fr.lorteau.skloop.foregroundservice.action.lostcontact";
        public static string RENEWED_CONTACT_ACTION = "fr.lorteau.skloop.foregroundservice.action.renewedcontact";
        public static string GOT_MESSAGE_ACTION = "fr.lorteau.skloop.foregroundservice.action.gotmessage";
        public static string SERIALIZED_MESSAGE_INTENT_EXTRA = "fr.lorteau.skloop.foregroundservice.intent.serialized_message";
        public static string GUID_TO_REMOVE_INTENT_EXTRA = "fr.lorteau.skloop.foregroundservice.intent.guid_to_remove";
        public static string GUID_TO_HIGHLIGHT_INTENT_EXTRA = "fr.lorteau.skloop.foregroundservice.intent.guid_to_highlight";
        public static int NEW_MESSAGE_NOTIFICATION_ID = 1;
        public static int LOST_CONTACT_NOTIFICATION_ID = 2;
        public static int FOREGROUND_SERVICE_NOTIFICATION_ID = 3;
        public static string SHARED_PREFERENCES_FILE_NAME = "fr.lorteau.skloop.preferences";
        public static string ACTIVITY_RUNNING_SHARED_PREFERENCE = "fr.lorteau.skloop.preferences.activity_running";
        public static string MUTE_SHARED_PREFERENCE = "fr.lorteau.skloop.preferences.mute";
        public static string LOST_CONTACT_SHARED_PREFERENCE = "fr.lorteau.skloop.preferences.lost_contact";
    }
}

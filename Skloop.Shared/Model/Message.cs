﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using Android.App;
using Android.Content;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Skloop.Model
{
    [DataContract]
    public class Message
    {
        private SourceApp _source;
        private string _from;
        private string _subject;
        private string _sent;
        private string _guid;

        public SourceApp Source
        {
            get { return _source; }
            set { _source = value; }
        }

        public string From
        {
            get { return _from; }
            set { _from = value; }
        }

        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }

        public string Sent
        {
            get { return _sent; }
            set { _sent = value; }
        }

        public string GUID
        {
            get { return _guid; }
            set { _guid = value; }
        }

        public Message()
        {
            _source = SourceApp.Undefined;
            _from = string.Empty;
            _subject = string.Empty;
            _sent = string.Empty;
        }

        public Message (SourceApp source, string from, string subject, string sent)
        {
            _source = source;
            _from = from;
            _subject = subject;
            _sent = sent;
        }

        public override string ToString()
        {
            string s = "Source: " + _source.ToString() + Environment.NewLine;
            if (!string.IsNullOrEmpty(_from)) s += "From: " + _from + Environment.NewLine;
            s += "Subject: " + _subject + Environment.NewLine + "Sent: " + _sent;
            return s;
        }
    }

    public class Messages : List<Message>
    {
        private static string MESSAGES_FILE_NAME = "messages_cache.xml";

        public event EventHandler<ItemRemovedEventArgs> ItemRemoved;

        public new void RemoveAt(int i)
        {
            try
            {
                base.RemoveAt(i);
                ItemRemoved?.Invoke(this, new ItemRemovedEventArgs(i));
            }
            catch { }
        }

        public void RemoveByGUID(string guid)
        {
            if (!string.IsNullOrEmpty(guid))
            {
                for (int i = 0; i < Count; i++)
                {
                    if (this[i].GUID == guid)
                    {
                        RemoveAt(i);
                        break;
                    }
                }
            }
        }

        public void SaveToAndroidStorage(Context context)
        {
            Java.IO.File file = new Java.IO.File(context.GetExternalFilesDir(null), MESSAGES_FILE_NAME);
            Java.IO.FileOutputStream outStream = null;
            try
            {
                string s = ObjectSerializer<Messages>.ToXml(this);
                outStream = new Java.IO.FileOutputStream(file);
                outStream.Write(System.Text.Encoding.UTF8.GetBytes(s));
            }
            catch { }
            finally { if (outStream != null) outStream.Close(); }
        }

        public void LoadFromAndroidStorage(Context context)
        {
            Java.IO.FileInputStream inStream = null;
            try
            {
                Java.IO.File file = new Java.IO.File(context.GetExternalFilesDir(null), MESSAGES_FILE_NAME);
                int length = (int)file.Length();
                byte[] bytes = new byte[length];
                inStream = new Java.IO.FileInputStream(file);
                inStream.Read(bytes);
                string s = System.Text.Encoding.UTF8.GetString(bytes);
                Messages m = ObjectSerializer<Messages>.FromXml(s);
                if (Count > 0) Clear();
                foreach (Model.Message mess in m)
                {
                    Add(mess);
                }
            }
            catch { }
            finally { if (inStream != null) inStream.Close(); }
        }

        public class ItemRemovedEventArgs : EventArgs
        {
            public int Index { get; internal set; }
            public ItemRemovedEventArgs(int index)
            {
                Index = index;
            }
        }
    }

    public enum SourceApp { Outlook, Lync, Reminder, Heartbeat, Undefined }
}

﻿//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using Android.OS;
using Android.Util;
using Skloop.Model;
using System;
using Android.Content;
using Skloop.Android.UI;
using Android.Views;
using Android.Widget;
using Skloop.Shared.Model;
using Android.Support.V7.Widget;
using Android.App;
using Android.Support.V4.Widget;
using Android.Support.V4.View;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using AlertDialog = Android.Support.V7.App.AlertDialog;
using Android.Content.PM;
using Process = Android.OS.Process;
using Uri = Android.Net.Uri;

namespace Skloop.Android
{
    [Activity(Label = "Skloop", MainLauncher = true, Icon = "@drawable/ic_launcher", LaunchMode = LaunchMode.SingleTask)]
    public class MainActivity : AppCompatActivity
    {
        public static MainActivity Instance { get; internal set; }
        public FrameLayout LostContactFrame { get; internal set; }

        private static string LOG_TAG = "Skloop: MainActivity";
        public Messages Messages { get; internal set; }
        private RecyclerView recyclerView;
        private RecyclerView.LayoutManager layoutManager;
        private MessagesAdapter adapter;
        private SwipeToDismiss swipeToDismiss;
        private DrawerLayout drawerLayout;
        private NavigationView navigationView;
        private Snackbar allahSnackBar;
        private MyBroadcastReceiver broadcastReceiver;

        private bool mute
        {
            get
            {
                var sp = GetSharedPreferences(Constants.SHARED_PREFERENCES_FILE_NAME, FileCreationMode.Private);
                bool b = sp.GetBoolean(Constants.MUTE_SHARED_PREFERENCE, false);
                return b;
            }
            set
            {
                var sp = GetSharedPreferences(Constants.SHARED_PREFERENCES_FILE_NAME, FileCreationMode.Private);
                var editor = sp.Edit();
                editor.PutBoolean(Constants.MUTE_SHARED_PREFERENCE, value);
                editor.Commit();
            }
        }

        public MainActivity()
        {
            Instance = this;
            Messages = new Messages();
            Messages.ItemRemoved += ((object sender, Messages.ItemRemovedEventArgs e) =>
            {
                if (adapter != null) adapter.NotifyItemRemoved(e.Index);
                Messages.SaveToAndroidStorage(this);
            });
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Window window = this.Window;
            window.ClearFlags(WindowManagerFlags.TranslucentStatus);
            window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            window.SetStatusBarColor(Resources.GetColor(Resource.Color.themeDark));
            SetContentView(Resource.Layout.Main);

            //setup messages list
            recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            layoutManager = new LinearLayoutManager(this);
            adapter = new MessagesAdapter(Messages);
            recyclerView.SetAdapter(adapter);
            recyclerView.SetLayoutManager(layoutManager);
            swipeToDismiss = new SwipeToDismiss(recyclerView);
            Messages.LoadFromAndroidStorage(this);
            adapter.NotifyDataSetChanged();
            if (!string.IsNullOrEmpty(Intent.GetStringExtra(Constants.GUID_TO_HIGHLIGHT_INTENT_EXTRA)))
            { 
                //TODO: highlight item
            }

            //setup action bar
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu_black_24dp);
  
            //setup lost contact notification
            LostContactFrame = FindViewById<FrameLayout>(Resource.Id.frameLayout1);
            var sp = GetSharedPreferences(Constants.SHARED_PREFERENCES_FILE_NAME, FileCreationMode.Private);
            bool b = sp.GetBoolean(Constants.LOST_CONTACT_SHARED_PREFERENCE, false);
            if (!b) LostContactFrame.Visibility = ViewStates.Gone;

            //setup side bar
            PackageInfo pInfo = PackageManager.GetPackageInfo(PackageName, 0);
            string version = pInfo.VersionName;
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.NavigationItemSelected += NavigationView_NavigationItemSelected;
            View headerLayout = navigationView.GetHeaderView(0);
            TextView appnameTextView = headerLayout.FindViewById<TextView>(Resource.Id.appNameTextView);
            appnameTextView.Text = "Skloop " + version;
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.navigation_drawer);

            //start service
            Intent startIntent = new Intent(this, typeof(ForegroundService));
            startIntent.SetAction(Constants.STARTFOREGROUND_ACTION);
            StartService(startIntent);
    }

        private void NavigationView_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            switch (e.MenuItem.ItemId)
            {
                case Resource.Id.nav_license:
                    LayoutInflater inflater = LayoutInflater.From(this);
                    View view = inflater.Inflate(Resource.Layout.ScrollableLayout, null);
                    TextView textview = (TextView)view.FindViewById(Resource.Id.textmsg);
                    textview.Text = Constants.LICENSE;
                    new AlertDialog.Builder(new ContextThemeWrapper(this, Android.Resource.Style.Theme_LightGreen))
                        .SetPositiveButton("OK", (s, args) => { })
                        .SetView(view)
                        .SetTitle("MIT License")
                        .Show();
                    break;
                case Resource.Id.nav_sourcecode:
                    var uri = Uri.Parse(Constants.SOURCECODE_LINK);
                    var i = new Intent(Intent.ActionView, uri);
                    StartActivity(i);
                    break;
                case Resource.Id.shutdown:
                    Shutdown(true);
                    break;
            }
            drawerLayout.CloseDrawers();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Layout.MainMenu, menu);
            View muteView = menu.FindItem(Resource.Id.action_toolbar_mute).ActionView;
            Switch muteSwitch = muteView.FindViewById<Switch>(Resource.Id.muteButton);
            if (mute) muteSwitch.Checked = true;
            muteSwitch.CheckedChange += (sender, e) =>
            {
                if (e.IsChecked)
                {
                    allahSnackBar = Snackbar.Make(drawerLayout, "All notifications suppressed", Snackbar.LengthIndefinite);
                    allahSnackBar.Show();
                    mute = true;
                }
                else
                {
                    if (allahSnackBar != null) allahSnackBar.Dismiss();
                    mute = false;
                }
            };
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.action_toolbar_clear_all:
                    new AlertDialog.Builder(new ContextThemeWrapper(this, Android.Resource.Style.Theme_LightGreen))
                        .SetPositiveButton("Yes", (sender, args) =>
                        {
                            Messages.Clear();
                            adapter.NotifyDataSetChanged();
                            Messages.SaveToAndroidStorage(this);
                        })
                        .SetNegativeButton("No", (sender, args) => { })
                        .SetMessage("Clear all notifications?")
                        .SetTitle("Confirmation")
                        .Show();
                    return true;
                case 16908332: //should be equal to Resource.Id.home but somehow it's not
                    drawerLayout.OpenDrawer(GravityCompat.Start);
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            IntentFilter filter = new IntentFilter(Constants.LOST_CONTACT_ACTION);
            filter.AddCategory(Intent.CategoryDefault);
            broadcastReceiver = new MyBroadcastReceiver();
            RegisterReceiver(broadcastReceiver, filter);
            Messages.LoadFromAndroidStorage(this);
        }

        protected override void OnDestroy()
        {
            try { UnregisterReceiver(broadcastReceiver); }
            catch { }
            base.OnDestroy();
        }

        protected override void OnPause()
        {
            try { UnregisterReceiver(broadcastReceiver); }
            catch { }
            base.OnPause();
        }

        private void Shutdown(bool confirm)
        {
            if (confirm)
            {
                new AlertDialog.Builder(new ContextThemeWrapper(this, Android.Resource.Style.Theme_LightGreen))
                        .SetPositiveButton("Shutdown", (sender, args) =>
                        {
                            Shutdown(false);
                        })
                        .SetNegativeButton("Cancel", (sender, args) => { })
                        .SetMessage("This will stop all monitoring and notifications from this app until it's started again.")
                        .SetTitle("Are you sure?")
                        .Show();
            }
            else
            {
                Intent stopIntent = new Intent(this, typeof(ForegroundService));
                stopIntent.SetAction(Constants.STOPFOREGROUND_ACTION);
                StartService(stopIntent);
                Process.KillProcess(Process.MyPid());
            }
        }

        public void GotSerializedMessage(string serialized)
        {
            try
            {
                Model.Message m = ObjectSerializer<Model.Message>.FromXml(serialized);
                if (m.Source != SourceApp.Heartbeat)
                {
                    if (string.IsNullOrEmpty(m.GUID)) m.GUID = Guid.NewGuid().ToString();
                    Log.Info(LOG_TAG, "Deserialized message:\n" + m.ToString());
                    Messages.Insert(0, m);
                    RunOnUiThread(() =>
                    {
                        adapter.NotifyItemInserted(Messages.Count - 1);
                    });
                }
            }
            catch (Exception oops)
            {
                Log.Error(LOG_TAG, "Failed to read message: " + oops.Message);
            }
        }

        protected override void OnStart()
        {
            base.OnStart();
            //note down that we started in a place where the service has access
            var sp = GetSharedPreferences(Constants.SHARED_PREFERENCES_FILE_NAME, FileCreationMode.WorldWriteable);
            var editor = sp.Edit();
            editor.PutBoolean(Constants.ACTIVITY_RUNNING_SHARED_PREFERENCE, true);
            editor.Commit();
        }

        protected override void OnStop()
        {
            base.OnStop();
            //note down that we stopped in a place where the service has access
            var sp = GetSharedPreferences(Constants.SHARED_PREFERENCES_FILE_NAME, FileCreationMode.WorldWriteable);
            var editor = sp.Edit();
            editor.PutBoolean(Constants.ACTIVITY_RUNNING_SHARED_PREFERENCE, false);
            editor.Commit();
        }

    }

    [BroadcastReceiver]
    public class MyBroadcastReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            if (intent.Action == Constants.LOST_CONTACT_ACTION)
            {
                MainActivity.Instance.LostContactFrame.Visibility = ViewStates.Visible;
            }
            else if (intent.Action == Constants.RENEWED_CONTACT_ACTION)
            {
                MainActivity.Instance.LostContactFrame.Visibility = ViewStates.Gone;
            }
            else if (intent.Action == Constants.GOT_MESSAGE_ACTION)
            {
                string serialized = intent.GetStringExtra(Constants.SERIALIZED_MESSAGE_INTENT_EXTRA);
                MainActivity.Instance.GotSerializedMessage(serialized);
            }
            else if (intent.Action == Constants.DISMISS_ACTION)
            {
                string guid = intent.GetStringExtra(Constants.GUID_TO_REMOVE_INTENT_EXTRA);
                MainActivity.Instance.Messages.RemoveByGUID(guid);
            }
        }
    }
}


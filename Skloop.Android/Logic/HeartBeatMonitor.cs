//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using System;
using System.Threading;
using Android.Util;

namespace Skloop.Android.Logic
{
    public class HeartBeatMonitor : IDisposable
    {
        private int beatsMissed;
        private int expectedDelay;
        private int tolerance; // how many beats is it ok to miss
        private bool stop;
        private static string LOG_TAG = "Skloop: HeartBeatMonitor";
        public event EventHandler HesDeadJim;
        public event EventHandler HesBack;

        public HeartBeatMonitor(int expectedDelay, int tolerance)
        {
            this.expectedDelay = expectedDelay;
            this.tolerance = tolerance;
            beatsMissed = 0;
            stop = false;
            new Thread(delegate ()
            {
                while(!stop)
                {
                    Thread.Sleep((expectedDelay + 1) * 1000);
                    if (beatsMissed > 0) Log.Debug(LOG_TAG, "Missed a beat");
                    if (++beatsMissed == tolerance + 1)
                    {
                        Log.Warn(LOG_TAG, "Lost contact with sender");
                        HesDeadJim?.Invoke(this, null);
                    }
                }
            }).Start();
        }

        public void GotHeartBeat()
        {
            if (beatsMissed > tolerance)
            {
                Log.Info(LOG_TAG, "Got contact back");
                HesBack?.Invoke(this, null);
            }
            beatsMissed = 0;
        }

        public void Dispose()
        {
            stop = true;
        }
    }
}
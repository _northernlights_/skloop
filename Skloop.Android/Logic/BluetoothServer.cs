//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using System;

using Android.Bluetooth;
using Java.Util;
using Android.Util;
using Java.Lang;
using Java.IO;
using Skloop.Shared.Model;

namespace Skloop.Android.Logic
{
    public class BluetoothServer : Thread
    {
        private BluetoothServerSocket serverSocket;
        private BluetoothAdapter bluetoothAdapter;
        private static string NAME = "Buters";
        private static string LOG_TAG = "Skloop BTServer";
        public static BluetoothServer Instance { get; internal set; }
        public bool Ready
        {
            get
            {
                if (serverSocket != null) return true;
                else return false;
            }
        }

        public event EventHandler<GotSerializedMessageEventArgs> GotSerializedMessage;

        public BluetoothServer()
        {
            Instance = this;
            BluetoothServerSocket tmp = null;
            try
            {
                bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
                if (bluetoothAdapter == null)
                {
                    // Device does not support Bluetooth
                    // TODO: notify user
                    Log.Error(LOG_TAG, "Device does not support bluetooth");
                }
                else
                {
                    tmp = bluetoothAdapter.ListenUsingRfcommWithServiceRecord(NAME, UUID.FromString(Constants.BLUETOOTH_SERVER_GUID));
                }
            }
            catch (Java.IO.IOException) { }
            serverSocket = tmp;
        }

        public override void Run()
        {
            BluetoothSocket socket = null;
            Log.Info(LOG_TAG, "Listening for connections...");
            // Keep listening until exception occurs or a socket is returned
            while (true)
            {
                try
                {
                    socket = serverSocket.Accept();
                }
                catch (Java.IO.IOException)
                {
                    Log.Error(LOG_TAG, "!!Stopped listening!!");
                    break;
                }
                // If a connection was accepted
                if (socket != null)
                {
                    new ConnectedThread(socket).run();
                }
            }
        }

        private class ConnectedThread : Thread
        {
            private BluetoothSocket socket;
            private System.IO.Stream inputStream;

            public ConnectedThread(BluetoothSocket socket)
            {
                this.socket = socket;
                System.IO.Stream tmpIn = null;

                try
                {
                    tmpIn = socket.InputStream;
                }
                catch (Java.IO.IOException) { }

                inputStream = tmpIn;
            }

            public void run()
            {
                bool stopWorker = false;

                while (!Thread.CurrentThread().IsInterrupted && !stopWorker)
                {
                    Java.Lang.StringBuilder sb = null;
                    try
                    {
                        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                        sb = new Java.Lang.StringBuilder(4096);// was: sb = new StringBuilder(inputStream.available());
                        string line;

                        while ((line = r.ReadLine()) != null)
                        {
                            sb.Append(line).Append('\n');
                        }

                    }
                    catch (IOException)
                    {
                        if (sb != null)
                        {
                            string serializedMessage = sb.ToString();
                            Log.Debug(LOG_TAG, "Received serialized message:\n" + serializedMessage);

                            BluetoothServer.Instance.RaiseGotSerializedMessage(serializedMessage);
                        }
                        stopWorker = true;
                    }

                }
                cancel();
            }

            /* Call this from the main activity to shutdown the connection */
            public void cancel()
            {
                try
                {
                    socket.Close();
                }
                catch (IOException) { }
            }
        }

        public void RaiseGotSerializedMessage(string s)
        {
            GotSerializedMessage?.Invoke(this, new GotSerializedMessageEventArgs(s));
        }

        public class GotSerializedMessageEventArgs : EventArgs
        {
            public string SerializedMessage { get; internal set; }
            public GotSerializedMessageEventArgs(string s)
            {
                SerializedMessage = s;
            }
        }
    }

}
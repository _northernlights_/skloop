//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Skloop.Shared.Model;
using Android.Graphics;
using Android.Support.V4.App;
using Android.Util;
using Skloop.Model;
using Skloop.Android.Logic;
using System;
using Skloop.Android.UI;

namespace Skloop.Android
{
    //http://www.truiton.com/2014/10/android-foreground-service-example/
    [Service]
    public class ForegroundService : Service
    {
        private static string LOG_TAG = "Skloop: Service";
        private BluetoothServer bluetoothServer;
        private HeartBeatMonitor heartBeatMonitor;
        private PendingIntent pendingIntent;
        private Messages messages;
        private Bitmap icon;

        private bool mute
        {
            get
            {
                var sp = GetSharedPreferences(Constants.SHARED_PREFERENCES_FILE_NAME, FileCreationMode.Private);
                bool b = sp.GetBoolean(Constants.MUTE_SHARED_PREFERENCE, false);
                return b;
            }
        }

        public override IBinder OnBind(Intent intent){ return null; }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            if (intent.Action.Equals(Constants.STARTFOREGROUND_ACTION))
            {
                Log.Info(LOG_TAG, "Starting");
                Intent notificationIntent = new Intent(this, typeof(MainActivity));
                notificationIntent.SetAction(Constants.MAIN_ACTION);
                notificationIntent.SetFlags(ActivityFlags.SingleTop);
                pendingIntent = PendingIntent.GetActivity(this, 0, notificationIntent, 0);

                icon = BitmapFactory.DecodeResource(Resources, Resource.Drawable.notification_icon);
                Notification notification = DefaultNotificationBuilder()
                    .SetContentText("Waiting for connection")
                    .SetSmallIcon(Resource.Drawable.ic_bluetooth_searching_white_18dp)
                    .Build();
                StartForeground(Constants.FOREGROUND_SERVICE_NOTIFICATION_ID, notification);

                messages = new Messages();
                messages.LoadFromAndroidStorage(this);
                if (bluetoothServer == null)
                {
                    bluetoothServer = new BluetoothServer();
                    if (bluetoothServer.Ready)
                    {
                        bluetoothServer.GotSerializedMessage += BluetoothServer_GotSerializedMessage;
                        bluetoothServer.Start();
                    }
                    else
                    {
                        NotificationCompat.Builder builder = DefaultNotificationBuilder()
                            .SetContentText("Device doesn't support bluetooth")
                            .SetSmallIcon(Resource.Drawable.ic_report_white_18dp);
                        if (!mute) builder = builder.SetDefaults(NotificationCompat.DefaultAll);
                        notification = builder.Build();
                        NotificationManager notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
                        notificationManager.Notify(Constants.FOREGROUND_SERVICE_NOTIFICATION_ID, notification);
                        //TODO communicate back to activity
                    }
                }
                if (heartBeatMonitor == null)
                {
                    heartBeatMonitor = new HeartBeatMonitor(Constants.HEARTBEATDELAY, 7);
                    heartBeatMonitor.HesDeadJim += HeartBeatMonitor_HesDeadJim;
                    heartBeatMonitor.HesBack += HeartBeatMonitor_HesBack;
                }
            }
            else if (intent.Action.Equals(Constants.DISMISS_ACTION))
            {
                string guid = intent.GetStringExtra(Constants.GUID_TO_REMOVE_INTENT_EXTRA);
                messages.RemoveByGUID(guid);
                messages.SaveToAndroidStorage(this);
                Broadcast(Constants.DISMISS_ACTION, guid);
                NotificationManager notifyMgr = (NotificationManager)GetSystemService(NotificationService);
                notifyMgr.Cancel(Constants.NEW_MESSAGE_NOTIFICATION_ID);
            }
            else if (intent.Action.Equals(Constants.STOPFOREGROUND_ACTION))
            {
                Log.Info(LOG_TAG, "Stopping");
                heartBeatMonitor.Dispose();
                bluetoothServer.Dispose();
                StopForeground(true);
                StopSelf();
            }
            return StartCommandResult.Sticky;
        }

        private void Broadcast(string action, string extra_string = null)
        {
            //check if activity is running before sending broadcast
            //the service crashes if we don't do that - I don't know why
            var sp = GetSharedPreferences(Constants.SHARED_PREFERENCES_FILE_NAME, FileCreationMode.WorldWriteable);
            bool running = sp.GetBoolean(Constants.ACTIVITY_RUNNING_SHARED_PREFERENCE, false);
            if (running)
            {
                Intent broadcastIntent = new Intent(this, typeof(MyBroadcastReceiver));
                broadcastIntent.SetAction(action);
                broadcastIntent.AddCategory(Intent.CategoryDefault);
                if (!string.IsNullOrEmpty(extra_string))
                {
                    if (action == Constants.GOT_MESSAGE_ACTION) //extra_string is actually a serialized message
                        broadcastIntent.PutExtra(Constants.SERIALIZED_MESSAGE_INTENT_EXTRA, extra_string);
                    else if (action == Constants.DISMISS_ACTION) //extra_string is the GUID of a message to remove
                        broadcastIntent.PutExtra(Constants.GUID_TO_REMOVE_INTENT_EXTRA, extra_string);
                }
                SendBroadcast(broadcastIntent);
            }
        }

        private void HeartBeatMonitor_HesBack(object sender, EventArgs e)
        {
            Notification notification = DefaultNotificationBuilder()
                            .SetContentText("Connected")
                            .SetSmallIcon(Resource.Drawable.ic_bluetooth_connected_white_18dp)
                            .Build();
            NotificationManager notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            notificationManager.Notify(Constants.FOREGROUND_SERVICE_NOTIFICATION_ID, notification);

            var sp = GetSharedPreferences(Constants.SHARED_PREFERENCES_FILE_NAME, FileCreationMode.Private);
            var editor = sp.Edit();
            editor.PutBoolean(Constants.LOST_CONTACT_SHARED_PREFERENCE, false);
            editor.Commit();
            Broadcast(Constants.RENEWED_CONTACT_ACTION);
        }

        private void HeartBeatMonitor_HesDeadJim(object sender, EventArgs e)
        {
            NotificationCompat.Builder builder = DefaultNotificationBuilder()
                .SetContentTitle("Skloop lost contact")
                .SetContentText("Sender might not be within range")
                .SetSmallIcon(Resource.Drawable.ic_report_white_18dp);
            if (!mute) builder = builder.SetDefaults(NotificationCompat.DefaultAll);
            Notification notification = builder.Build();
            NotificationManager notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            notificationManager.Notify(Constants.FOREGROUND_SERVICE_NOTIFICATION_ID, notification);

            var sp = GetSharedPreferences(Constants.SHARED_PREFERENCES_FILE_NAME, FileCreationMode.Private);
            var editor = sp.Edit();
            editor.PutBoolean(Constants.LOST_CONTACT_SHARED_PREFERENCE, true);
            editor.Commit();
            Broadcast(Constants.LOST_CONTACT_ACTION);
        }

        private void BluetoothServer_GotSerializedMessage(object sender, BluetoothServer.GotSerializedMessageEventArgs e)
        {
            try
            {
                heartBeatMonitor.GotHeartBeat();
                Model.Message m = ObjectSerializer<Model.Message>.FromXml(e.SerializedMessage);
                if (m.Source != SourceApp.Heartbeat)
                {
                    if (string.IsNullOrEmpty(m.GUID)) m.GUID = Guid.NewGuid().ToString();
                    Log.Info(LOG_TAG, "Deserialized message:\n" + m.ToString());
                    messages.Insert(0, m);
                    messages.SaveToAndroidStorage(ApplicationContext);
                    Notify(m);

                    string reserialized = ObjectSerializer<Model.Message>.ToXml(m);
                    Broadcast(Constants.GOT_MESSAGE_ACTION, reserialized);
                }
                else
                {
                    Notification notification = DefaultNotificationBuilder()
                            .SetContentText("Connected")
                            .SetSmallIcon(Resource.Drawable.ic_bluetooth_connected_white_18dp)
                            .Build();
                    NotificationManager notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
                    notificationManager.Notify(Constants.FOREGROUND_SERVICE_NOTIFICATION_ID, notification);
                }
            }
            catch (Exception oops)
            {
                Log.Error(LOG_TAG, "Failed to read message: " + oops.Message);
            }
        }

        private NotificationCompat.Builder DefaultNotificationBuilder()
        {
            return new NotificationCompat.Builder(this)
                            .SetContentTitle("Skloop")
                            .SetTicker("Lync/Skype for Business and Outlook monitoring")
                            .SetLargeIcon(Bitmap.CreateScaledBitmap(icon, 128, 128, false))
                            .SetContentIntent(pendingIntent)
                            .SetOngoing(true);
        }

        private void Notify(Model.Message message)
        {
            if (!mute)
            {
                string contentTitle = "New ";
                if (message.Source == SourceApp.Outlook) contentTitle += "email";
                else if (message.Source == SourceApp.Lync) contentTitle += "conversation";
                else if (message.Source == SourceApp.Reminder) contentTitle += "reminder";

                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(this)
                                .SetSmallIcon(Resource.Drawable.ic_stat_communication_chat)
                                .SetContentTitle(contentTitle)
                                .SetPriority(NotificationCompat.PriorityMax)
                                .SetCategory(NotificationCompat.CategoryMessage);
                if (!mute) builder.SetDefaults(NotificationCompat.DefaultAll);
                if (!string.IsNullOrEmpty(message.From))
                    builder.SetContentText("From: " + message.From + "\nSubject: " + message.Subject);
                else builder.SetContentText("Subject: " + message.Subject);

                Intent dismissIntent = new Intent(this, this.Class);
                dismissIntent.SetAction(Constants.DISMISS_ACTION);
                dismissIntent.PutExtra(Constants.GUID_TO_REMOVE_INTENT_EXTRA, message.GUID);
                PendingIntent dismissPendingIntent = PendingIntent.GetService(this, 0, dismissIntent, PendingIntentFlags.UpdateCurrent);
                builder.AddAction(Resource.Drawable.ic_check_black_24dp, "Dismiss", dismissPendingIntent);

                Intent notificationIntent = new Intent(this, typeof(MainActivity));
                notificationIntent.SetAction(Constants.MAIN_ACTION);
                notificationIntent.SetFlags(ActivityFlags.SingleTop);
                notificationIntent.PutExtra(Constants.GUID_TO_HIGHLIGHT_INTENT_EXTRA, message.GUID);
                PendingIntent pIntent = PendingIntent.GetActivity(this, 0, notificationIntent, 0);
                builder.SetContentIntent(pIntent);

                NotificationManager notifyMgr = (NotificationManager)GetSystemService(NotificationService);
                notifyMgr.Notify(Constants.NEW_MESSAGE_NOTIFICATION_ID, builder.Build());
            }
        }
    }

}
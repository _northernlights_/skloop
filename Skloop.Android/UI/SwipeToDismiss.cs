//
// The MIT License (MIT)
//
//Copyright (c) 2016 Clem Lorteau <clem@lorteau.fr>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

using Android.Support.V7.Widget;
using Android.Support.V7.Widget.Helper;

namespace Skloop.Android.UI
{
    //https://github.com/nemanja-kovacevic/recycler-view-swipe-to-delete/blob/master/app/src/main/java/net/nemanjakovacevic/recyclerviewswipetodelete/MainActivity.java
    //http://stackoverflow.com/a/30601554/571572
    public class SwipeToDismiss
    {
        public SwipeToDismiss(RecyclerView recyclerView)
        {
            SwipeCallback swipeCallback = new SwipeCallback(0, ItemTouchHelper.Left | ItemTouchHelper.Right,
                recyclerView);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeCallback);
            itemTouchHelper.AttachToRecyclerView(recyclerView);
        }

        public class SwipeCallback : ItemTouchHelper.SimpleCallback
        {
            private int _swipeDirs;
            private RecyclerView _recyclerView;

            public SwipeCallback(int dragDirs, int swipeDirs, RecyclerView recyclerView) : base(dragDirs, swipeDirs)
            {
                _swipeDirs = swipeDirs;
                _recyclerView = recyclerView;
            }

            public override bool OnMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) { return true; }

            public override void OnSwiped(RecyclerView.ViewHolder viewHolder, int direction)
            {
                MessagesAdapter adapter = (MessagesAdapter)_recyclerView.GetAdapter();
                adapter.Messages.RemoveAt(viewHolder.AdapterPosition);
            }
        }
    }
}